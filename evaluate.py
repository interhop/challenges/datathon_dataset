import pandas as pd
import numpy as np
import argparse


def evaluate_links(gold_path, result_path, verbose=1):
    gold = pd.read_csv(gold_path)
    result = pd.read_csv(result_path)
    TP = []
    FP = []
    TN = []
    FN = []
    for line in result['patient_ix'].values:
        # predicted link
        if len(line.split(',')) >= 2:
            if line in gold['patient_ix'].values:
                TP.append(line)
            else:
                FP.append(line)
        # predicted non link
        else:
            if line in gold['patient_ix'].values:
                TN.append(line)
            else:
                FN.append(line)
    return TP, FP, TN, FN


def compute_score(gold_path, result_path, verbose=1):
    tp, fp, tn, fn = evaluate_links(gold_path, result_path, verbose=verbose)
    precision = len(tp) / (len(tp) + len(fp))
    recall = len(tp) / (len(tp) + len(fn))
    f1 = 2*precision*recall/(precision+recall)
    if verbose == 1:
        print('Precision={:.4f} | Recall={:.4f} | F1={:.4f}'.format(precision, recall, f1))
    return precision, recall, f1


if __name__ == '__main__':
    print('-----Evaluation of deduplication result-----')
    parser = argparse.ArgumentParser(
        description='Compute precision/recall/f1 scores for a result file against a gold standard.\n\t Both gold and result should be csv files with a unique column named patient_ix containing one line for each deduplicated patient. For one given patient, its possible multiple ids are comma separated.')
    
    parser.add_argument('--gold_path', type=str,
                        help='gold standard path')
    parser.add_argument('--result_path', type=str,
                        help='result path')
    parser.add_argument('--verbose', type=int, default=1,
                        help='Make it 0 to hide printing')
    
    args = parser.parse_args()
    compute_score(args.gold_path, args.result_path, args.verbose)